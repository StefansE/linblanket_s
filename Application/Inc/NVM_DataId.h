/*
 * NVM_DataId.h
 *
 *  Created on: 24.02.2019
 *      Author: ewars
 */

#ifndef MYTASKS_NVM_DATAID_H_
#define MYTASKS_NVM_DATAID_H_


/*
 * 		Enumerator listing all NVM data entities
 *
 *		It also defines NVM data Id number
 *
 *		Currently 8 bit value - 256 NVM entities possible
 * */

typedef enum {
	nvm_DeviceID = 0,
	nvm_DeviceType = 1,
	nvm_OutputInitValue = 2,
	nvm_LED1_InitValue = 3,
	nvm_LED2_InitValue = 4,
	nvm_DefaultMode = 5,
	nvm_ResistorTrim = 6,
	NVM_EntryCount
} NVM_DataId_e;

#endif /* MYTASKS_NVM_DATAID_H_ */

/*
 * NVM.h
 *
 *  Created on: 22.02.2019
 *      Author: ewars
 */

#ifndef MYTASKS_NVM_H_
#define MYTASKS_NVM_H_

#include "inttypes.h"
#include "NVM_DataId.h"

typedef struct {
	NVM_DataId_e 	nvm_Id;
	uint8_t 		nvm_extnd;
	uint16_t		nvm_data16;
} NVM_Entry_t;


void 	NVM_InitNVM(void);
void 	NVM_ReadAll(NVM_Entry_t * table);
void	NVM_ReadEntryById(NVM_DataId_e entryId, uint16_t * value);
void 	NVM_ReadExtendedData(NVM_DataId_e entryId, uint8_t * value);
void 	NVM_WriteDataWord(NVM_DataId_e entryId, uint8_t extId, uint16_t * data16);
uint8_t NVM_GetDeviceEntryIndex(uint8_t device);

/***********************************************************
 * 			Reserved for advanced user
 * *********************************************************/
void 	NVM_ClearAllPages(void);

#endif /* MYTASKS_NVM_H_ */

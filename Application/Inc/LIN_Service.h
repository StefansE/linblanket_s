/*
 * LIN_Service.h
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

#ifndef INC_LIN_SERVICE_H_
#define INC_LIN_SERVICE_H_

#define LIN_RX_BUFFER_MAX_SIZE				10
#define LIN_DEFAULT_RESPONSE_LEN		4

typedef enum {
	seq_Led1_duty = 0,
	seq_Led2_duty,
	seq_Dev_duty,
	seq_reserved,
	seq_MAX
} setDuty_e;

typedef struct{
	uint8_t StartByte;
	uint8_t Synchronization_Byte_u8;
	uint8_t Identifier_u8;
	uint8_t MsgSize_u8;
	uint8_t Command_u8;
	uint8_t Checksum_u8;
}LIN_header_t;


void LIN_ReceiveMessage(void);

#endif /* INC_LIN_SERVICE_H_ */

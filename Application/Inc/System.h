/*
 * System.h
 *
 *  Created on: Dec 23, 2023
 *      Author: ewars
 */

#ifndef INC_SYSTEM_H_
#define INC_SYSTEM_H_

typedef enum {
	adc_SupplyVoltage_33 = 0,
	adc_MainsVoltage,
	adc_Temperature,
	adc_Reference,
	adc_MAX
} adcResults_e;

void sys_InitADC(void);
void sys_ADC_handler(void);
void sys_ToggleLed(device_e device);
uint8_t sys_getTemperature(void);

#endif /* INC_SYSTEM_H_ */

/*
 * AppStatus.h
 *
 *  Created on: 27 gru 2022
 *      Author: ewars
 */

#ifndef INC_APPSTATUS_H_
#define INC_APPSTATUS_H_

#define CHIP_ID_BASE_ADDRESS		0x1FFFF7AC
#define CHIP_ID_REG_OFFSET			4

#define APP_SET_BIT(REG, BIT)     	((REG) |= (1 << BIT))

#define APP_CLEAR_BIT(REG, BIT)   	((REG) &= ~(1 << BIT))

#define APP_CHECK_BIT(REG, BIT)		(REG & (1<<BIT))


#define SR1_BITFIELD_MOTOR            (0x01U << 0);
#define SR1_BITFIELD_L1S              (0x01U << 1);
#define SR1_BITFIELD_L2S              (0x01U << 2);
#define SR1_BITFIELD_EF               (0x01U << 3);
#define SR1_BITFIELD_ERR1             (0x01U << 4);
#define SR1_BITFIELD_ERR2             (0x01U << 5);
#define SR1_BITFIELD_ERR3             (0x01U << 6);
#define SR1_BITFIELD_ERR4             (0x01U << 7);


typedef enum {
	APP_NOK,
	APP_OK
} fxnResp_e;

typedef enum {
	appMain_Init = 0,
	appMain_Run,
	appMain_Halt,
	appMain_Error,
	AppMain_MAX
} appMainState_e;

typedef enum {
	Motor_dev = 0,
	Led1_dev,
	Led2_dev,
	DeviceMax
} device_e;

typedef struct {
	TIM_HandleTypeDef* 	timer_p;
	uint16_t			timerChannel;
} devList_t;

/*
SR1_bitfields{
	uint8_t MS 	:1; // Motor status
	uint8_t L1S :1; // LED1 status
	uint8_t L2S :1; // LED2 status
	uint8_t EF	:1; // Error Flag
	uint8_t ER1 :1; // Error Register 1
	uint8_t ER2 :1; // Error Register 2
	uint8_t ER3 :1; // Error Register 3
	uint8_t ER4 :1; // Error Register 4

}
*/
typedef struct {
	uint8_t 	myAdress;
	union{
		struct{
			uint8_t	motor 	:1;
			uint8_t	led1	:1;
			uint8_t	led2	:1;
			uint8_t	err1	:1;
			uint8_t	err2	:1;
			uint8_t	err3	:1;
			uint8_t	err4	:1;
			uint8_t	err5	:1;
		}SR1;
		uint8_t 	SR1_bitfields;
	}status;
	uint8_t 	dutyCycles[3];
	uint16_t	batVoltage;
	uint16_t	supVoltage;
	uint8_t		deviceRole; //1: Motor; 2:Coil

} appStatus_s;


fxnResp_e App_SetDuty(device_e dev, uint8_t duty);
fxnResp_e App_StartDevice(device_e dev);
fxnResp_e App_StopDev(device_e dev);
fxnResp_e App_STOP_ALL(void);

void App_setDeviceStatus(device_e device, uint8_t duty);
fxnResp_e App_getDeviceStatus(device_e device);
uint8_t App_getDeviceDutyCurrent(device_e device);
uint8_t App_getDeviceDutyNVM(device_e device);


#endif /* INC_APPSTATUS_H_ */

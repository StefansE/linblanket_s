/*
 * Application.c
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

//#include "main.h"
//#include "dma.h"
#include "tim.h"
//#include "usart.h"
//#include "gpio.h"
#include "wwdg.h"
#include "stm32f0xx.h"


#include "Application.h"
#include "AppStatus.h"
#include "System.h"
#include "LIN_Service.h"
#include "NVM.h"
#include "NVM_DataId.h"

devList_t devList[] = {
		{&htim1,	TIM_CHANNEL_2},//Motor_dev
		{&htim3,	TIM_CHANNEL_1},//Led1_dev
		{&htim3,	TIM_CHANNEL_2} //Led2_dev
};

void App_NVMInitiateApp(void);
void App_DevStatusUpdate(void);
void App_UpdateLedStatus(void);

void App_DBG_MotorContol(int8_t* duty);


appStatus_s appStatus = {0};
appMainState_e appMainState = appMain_Init;
uint8_t appAdcSemaphore = 0;

int8_t dbg_motorSem = 0;


// Main application task
void MyTask(void){

	while(1){

		switch(appMainState){

		case appMain_Init:
			NVM_InitNVM();
			App_NVMInitiateApp();

			HAL_WWDG_Refresh(&hwwdg);

			sys_InitADC();

			App_SetDuty(Led2_dev, 0);
			appMainState = appMain_Run;

			LIN_ReceiveMessage();

			break;
		case appMain_Run:

			HAL_WWDG_Refresh(&hwwdg);
			App_UpdateLedStatus();

			App_DBG_MotorContol(&dbg_motorSem);

			if(appAdcSemaphore){
				appAdcSemaphore = 0;

				sys_ADC_handler();
			}
			break;
		case appMain_Error:
			break;
		case appMain_Halt:
			break;
		default:
			break;

		}
	}
}

void App_DBG_MotorContol(int8_t* duty){

	if((*duty > 0)&&(*duty < 100)){
		App_SetDuty(Motor_dev, *duty);
	}
	else if (*duty < 0){
		App_SetDuty(Motor_dev, 0);
	}

	*duty = 0;
}

// LED to be on when Motor is ON
void App_UpdateLedStatus(void){

	if(App_getDeviceStatus(Motor_dev) > 0){

		uint16_t duty = 0;

		NVM_ReadEntryById(NVM_GetDeviceEntryIndex(Led2_dev), &duty);

		App_setDeviceStatus(Led2_dev, (uint8_t) duty);
	}
	else{
		App_setDeviceStatus(Led2_dev, 0);
	}

}

void App_Tim14_Cbk(void){
	appAdcSemaphore = 1;
}


void App_NVMInitiateApp(void){

	// Reading settings from NVM

	//reading data from NVM
	uint16_t nvmTemp = 0;

	//device ID
	NVM_ReadEntryById(nvm_DeviceID, &nvmTemp);
	appStatus.myAdress = (uint8_t) nvmTemp;

	// Motor/Coil PWM initial setting
	NVM_ReadEntryById(nvm_OutputInitValue, &nvmTemp);
	appStatus.dutyCycles[Motor_dev] = (uint8_t) nvmTemp;

	// LED1 PWM initial setting
	NVM_ReadEntryById(nvm_LED1_InitValue, &nvmTemp);
	appStatus.dutyCycles[Led1_dev] = (uint8_t) nvmTemp;

	// LED2 PWM initial setting
	NVM_ReadEntryById(nvm_LED2_InitValue,&nvmTemp);
	appStatus.dutyCycles[Led2_dev] = (uint8_t) nvmTemp;

	// Motor/Coil device role
	NVM_ReadEntryById(nvm_DefaultMode,&nvmTemp);
	appStatus.deviceRole = (uint8_t) nvmTemp;

	// Setting PWM initial values according to NVM
	for(uint8_t dev = 0; dev < DeviceMax; dev++){
		App_SetDuty(dev, appStatus.dutyCycles[dev]);
	}


}

void App_setDeviceStatus(device_e device, uint8_t duty){

	if(duty > 0){
		APP_SET_BIT(appStatus.status.SR1_bitfields, device);
		App_SetDuty(device, (uint8_t) duty);

	}
	else{
		APP_CLEAR_BIT(appStatus.status.SR1_bitfields, device);
		App_SetDuty(device, 0);
	}
}

fxnResp_e App_getDeviceStatus(device_e device){
	if(appStatus.status.SR1_bitfields >> device & 0x01){
		return APP_OK;
	}
	else return APP_NOK;
}

uint8_t App_getDeviceDutyCurrent(device_e device){
	if(device < DeviceMax){
		return appStatus.dutyCycles[device];
	}
	else return 0;
}

uint8_t App_getDeviceDutyNVM(device_e device){
	uint16_t duty = 0;
	NVM_ReadEntryById(NVM_GetDeviceEntryIndex((uint8_t)device), &duty);

	return (uint8_t) duty;
}

fxnResp_e App_SetDuty(device_e dev, uint8_t duty){
	fxnResp_e retVal = APP_NOK;

	if(duty > 0){
		APP_SET_BIT(appStatus.status.SR1_bitfields, dev);
	}
	else{
		APP_CLEAR_BIT(appStatus.status.SR1_bitfields, dev);
	}

	if((duty >=0) && (duty <= 100)){
		// reversing duty since the MOSFET driving inverted logic
		if(dev == Motor_dev){
			duty = 100 - duty;
		}

		// resetting timer duty cycle
		TIM_SetDutyCycle(devList[dev].timer_p, devList[dev].timerChannel, duty);

		// updating application status register
		appStatus.dutyCycles[dev] = duty;

		retVal = APP_OK;
	}
	return retVal;
}

fxnResp_e App_StartDevice(device_e dev){
	fxnResp_e retVal = APP_NOK;

	if(APP_CHECK_BIT(appStatus.status.SR1_bitfields, dev) == 0){
		APP_SET_BIT(appStatus.status.SR1_bitfields, dev);
		TIM_SetDutyCycle(devList[dev].timer_p, devList[dev].timerChannel, appStatus.dutyCycles[dev]);
	}

	retVal = APP_OK;

	return retVal;
}

void App_DevStatusUpdate(void){
	for(device_e dev = 0; dev < DeviceMax; dev++){
		if(APP_CHECK_BIT(appStatus.status.SR1_bitfields, dev)){
			TIM_SetDutyCycle(devList[dev].timer_p, devList[dev].timerChannel, appStatus.dutyCycles[dev]);
		}
	}
}

fxnResp_e App_StopDev(device_e dev){
	fxnResp_e retVal = APP_NOK;

	App_SetDuty(dev, 0);
	retVal = APP_OK;

	return retVal;
}

fxnResp_e App_STOP_ALL(void){
	fxnResp_e retVal = APP_NOK;

	for(device_e i = Motor_dev; i < DeviceMax; i++){
		App_StopDev(i);
	}

	retVal = APP_OK;
	return retVal;
}

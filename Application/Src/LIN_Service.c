/*
 * LIN_Service.c
 *
 *  Created on: 22 gru 2022
 *      Author: ewars
 */

#include "usart.h"
#include "LIN_Service.h"
#include "LIN_command.h"
#include "AppStatus.h"
#include "System.h"
#include "NVM.h"

extern appStatus_s appStatus;

typedef struct {
	uint8_t command_ID;
	void (*fptr)(uint8_t * rxData);
} cmdHandler_s;

LIN_header_t RX_header = {0};
uint8_t LinRxDataBuffer[LIN_RX_BUFFER_MAX_SIZE] = {0};

cmdHandler_s linGetCmd[] = {
		{LIN_CMD_RESERVED, 			0},//to align with CMD numbers

		{LIN_CMD_GET_STATUS, 		LIN_GetStatus},
		{LIN_CMD_GET_MY_ADDRESS,	LIN_GetMyAddress},
		{LIN_CMD_GET_OUT_DUTY,		LIN_GetOutputInitDuty},
		{LIN_CMD_GET_LED1_DUTY,		LIN_GetLED1InitDuty},
		{LIN_CMD_GET_LED2_DUTY,		LIN_GetLED2InitDuty},
		{LIN_CMD_GET_BAT_VOLTAGE,	LIN_GetBatVoltage},
		{LIN_CMD_GET_SUP_VOLTAGE,	LIN_GetSupplyVoltage},
		{LIN_CMG_GET_CHIP_ID_0,		LIN_GetChipID_0},
		{LIN_CMG_GET_CHIP_ID_1,		LIN_GetChipID_1},
		{LIN_CMG_GET_CHIP_ID_2,		LIN_GetChipID_2},
		{LIN_CMD_GET_TEMPERATURE,	LIN_GetTemperature}
};

cmdHandler_s linSetCmd[] = {
		{LIN_CMD_SET_MOTOR_DUTY,	LIN_SetMotorDuty},
		{LIN_CMD_SET_LED1_DUTY,		LIN_SetLED1Duty},
		{LIN_CMD_SET_LED2_DUTY,		LIN_SetLED2Duty},
		{LIN_CMD_SET_MOTOR_INIT,	LIN_SetMotorInitDuty},
		{LIN_CMD_SET_LED1_INIT,		LIN_SetLED1InitDuty},
		{LIN_CMD_SET_LED2_INIT,		LIN_SetLED2InitDuty},
		{LIN_CMD_SET_MY_ADRESS,		LIN_SetMyAddress},
		{LIN_CMD_SET_MY_ROLE,		LIN_SetMyRole},
		{LIN_START_MOTOR,			LIN_StartMotor},
		{LIN_STOP_MOTOR,			LIN_StopMotor},
		{LIN_CMD_SET_RESISTOR_TRIM,	LIN_SetResistorTrim}
};

cmdHandler_s linZoneCmd[] = {
		{LIN_ZONE_START,			LIN_ZONE_Start},
		{LIN_ZONE_STOP,				LIN_ZONE_Stop}
};


#define GET_CMD_MIN_IDX				0
#define GET_CMD_MAX_IDX				(GET_CMD_MIN_IDX + (sizeof(linGetCmd)/sizeof(linGetCmd[0])))

#define SET_CMD_MIN_IDX				20
#define SET_CMD_MAX_IDX				(SET_CMD_MIN_IDX + (sizeof(linSetCmd)/sizeof(linSetCmd[0])))

#define ZONE_CMD_MIN_ID				80
#define ZONE_CMD_MAX_ID				(ZONE_CMD_MIN_ID + (sizeof(linZoneCmd)/sizeof(linZoneCmd[0])))

/* ----------- Function prototypes ------------------------ */
uint8_t LIN_ID_parity(uint8_t linID);
uint8_t LIN_DataChecksum(uint8_t * data_p, uint8_t len);


/* -------------------------------------------------------- */

void LIN_ReceiveMessage(void){

	/*	Receiving first 6 bytes:
	 * 	1. LIN break: expected 0
	 * 	2. LIN synchronization byte: expected 0x55 (85 decimal)
	 * 	3. Protected ID
	 * 	4. Message size
	 * 	5. Command ID
	 * 	6. Message checksum
	 */

	HAL_UART_Receive_IT(&huart1, (uint8_t *) &RX_header, 6);
}

void LIN_ReceiveTransmitted(uint8_t size){

	HAL_UART_Receive_IT(&huart1, (uint8_t *) &RX_header, size);

}

uint8_t LIN_DataChecksum(uint8_t * data_p, uint8_t len){
	uint16_t ckSum = 0;
	uint8_t i = 0;
	uint8_t * ptr = data_p;

	for(i = 0; i < len; i++){
		ckSum += *ptr;
		if(ckSum > 0xFF){
			ckSum -= 0xFF;
		}
		ptr++;
	}

	// XOR with FF - reverse
	ckSum = ckSum ^ 0xFF;

	return (uint8_t)ckSum;
}

uint8_t LIN_ID_parity(uint8_t linID){
	uint8_t parity = 0;
	uint8_t parL, parH;

	uint8_t parityLowBytes[] = {0, 1, 2, 4};
	uint8_t parityHighBytes[] = {1, 3, 4, 5};

	//Parity LOW bit
	parL = 0;
	for(uint8_t i = 0; i < 4; i++){
		if(linID & (1<<parityLowBytes[i])){
			parL++;
		}
	}
	//Parity High bit
	parH = 0;
	for(uint8_t i = 0; i < 4; i++){
		if(linID & (1<<parityHighBytes[i])){
			parH++;
		}
	}

	parH = !parH;

	parity = ((parL & 0x01)<<6) + ((parH & 0x01)<<7);

	return parity;
}

void HAL_UART_TxCpltCallback(UART_HandleTypeDef *huart)
{

  sys_ToggleLed(Led1_dev);

  LIN_ReceiveMessage();

}


void HAL_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{

	//check if the message is for me and has valid parity
	if(((RX_header.Identifier_u8 & 0x3F) == appStatus.myAdress) &&
			((RX_header.Identifier_u8 & 0xC0) == LIN_ID_parity(RX_header.Identifier_u8))){

		// Set command range
		if((RX_header.Command_u8 >= SET_CMD_MIN_IDX) && (RX_header.Command_u8 <= SET_CMD_MAX_IDX)){
			//get rest of the message
			HAL_UART_Receive(&huart1, &LinRxDataBuffer[0], RX_header.MsgSize_u8, 10);

			//check if message is valid
			// TODO

			//execute command
			linSetCmd[RX_header.Command_u8 - SET_CMD_MIN_IDX].fptr(&LinRxDataBuffer[0]);

		}

		// Get command range
		if((RX_header.Command_u8 >= GET_CMD_MIN_IDX) && (RX_header.Command_u8 <= GET_CMD_MAX_IDX)){
			linGetCmd[RX_header.Command_u8 - GET_CMD_MIN_IDX].fptr(&LinRxDataBuffer[0]);

			LIN_ReceiveTransmitted(LIN_DEFAULT_RESPONSE_LEN);

		}
	}

	else{
		// check if this is a mass message for my ZONE
		uint8_t myZone = appStatus.myAdress/10;
		uint8_t msgZone = (RX_header.Identifier_u8 & 0x3f)/10;

		// check if message is for my zone and has a valid parity
		if ((msgZone == myZone)&&
				((RX_header.Identifier_u8 & 0xC0) == LIN_ID_parity(RX_header.Identifier_u8))){

			// check if command is in the ZONE COMMAND range
			if((RX_header.Command_u8 >= ZONE_CMD_MIN_ID) && (RX_header.Command_u8 <= ZONE_CMD_MAX_ID)){
				linZoneCmd[RX_header.Command_u8 - ZONE_CMD_MIN_ID].fptr(&LinRxDataBuffer[0]);
			}
		}

		LIN_ReceiveMessage();

	}

}


/* --------------------------------------------------------
 * 					Getter Functions
 * ------------------------------------------------------- */
void LIN_GetStatus(uint8_t * data_p){
	HAL_UART_Transmit_IT(&huart1, &appStatus.status.SR1_bitfields, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetMyAddress(uint8_t * data_p){
	HAL_UART_Transmit_IT(&huart1, &appStatus.myAdress, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetLED1InitDuty(uint8_t * data_p){
	uint8_t resp[4] = {0};
	resp[0] = appStatus.dutyCycles[Led1_dev];
	HAL_UART_Transmit_IT(&huart1, &resp[0] , LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetLED2InitDuty(uint8_t * data_p){
	uint8_t resp[4] = {0};
	resp[0] = appStatus.dutyCycles[Led2_dev];
	HAL_UART_Transmit_IT(&huart1, &resp[0], LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetOutputInitDuty(uint8_t * data_p){
	uint8_t resp[4] = {0};
	resp[0] = appStatus.dutyCycles[Motor_dev];
	HAL_UART_Transmit_IT(&huart1, &resp[0], LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetBatVoltage(uint8_t * data_p){
	HAL_UART_Transmit_IT(&huart1, (uint8_t*) &appStatus.batVoltage, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetTemperature(uint8_t * data_p){
	uint8_t resp[4]= {0};
	resp[0] = sys_getTemperature();
	HAL_UART_Transmit_IT(&huart1, resp, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetSupplyVoltage(uint8_t * data_p){
	HAL_UART_Transmit_IT(&huart1, (uint8_t*) &appStatus.batVoltage, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetChipID_0(uint8_t * data_p){
	uint32_t id = *((uint32_t*) CHIP_ID_BASE_ADDRESS);
	HAL_UART_Transmit_IT(&huart1, (uint8_t*) &id, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetChipID_1(uint8_t * data_p){
	uint32_t id = *((uint32_t*) (CHIP_ID_BASE_ADDRESS + CHIP_ID_REG_OFFSET));
	HAL_UART_Transmit_IT(&huart1, (uint8_t*) &id, LIN_DEFAULT_RESPONSE_LEN);
}
void LIN_GetChipID_2(uint8_t * data_p){
	uint32_t id = *((uint32_t*) (CHIP_ID_BASE_ADDRESS + CHIP_ID_REG_OFFSET + CHIP_ID_REG_OFFSET));
	HAL_UART_Transmit_IT(&huart1, (uint8_t*) &id, LIN_DEFAULT_RESPONSE_LEN);
}

/* --------------------------------------------------------
 * 					Setter Functions
 * ------------------------------------------------------- */
void LIN_SetMotorDuty(uint8_t * data_p){
	App_SetDuty(Motor_dev, LinRxDataBuffer[seq_Dev_duty]);
}
void LIN_SetLED1Duty(uint8_t * data_p){
	App_SetDuty(Led1_dev, LinRxDataBuffer[seq_Led1_duty]);
}
void LIN_SetLED2Duty(uint8_t * data_p){
	App_SetDuty(Led2_dev, LinRxDataBuffer[seq_Led2_duty]);
}

void LIN_SetMotorInitDuty(uint8_t * data_p){
	uint16_t duty = *data_p;
	NVM_WriteDataWord(nvm_OutputInitValue, 0, &duty);
	appStatus.dutyCycles[Motor_dev] = *data_p;
}
void LIN_SetLED1InitDuty(uint8_t * data_p){
	uint16_t duty = *data_p;
	NVM_WriteDataWord(nvm_LED1_InitValue, 0, &duty);
	appStatus.dutyCycles[Led1_dev] = *data_p;
}
void LIN_SetLED2InitDuty(uint8_t * data_p){
	uint16_t duty = *data_p;
	NVM_WriteDataWord(nvm_LED2_InitValue, 0, &duty);
	appStatus.dutyCycles[Led2_dev] = *data_p;
}
void LIN_SetMyAddress(uint8_t * data_p){
	uint16_t address = *data_p;
	NVM_WriteDataWord(nvm_DeviceID, 0, &address);
	appStatus.myAdress = *data_p;
}
void LIN_SetMyRole(uint8_t * data_p){
	uint16_t role = *data_p;
	NVM_WriteDataWord(nvm_DefaultMode, 0, &role);
	appStatus.deviceRole = *data_p;
}

void LIN_StartMotor(uint8_t * data_p){
	App_StartDevice(Motor_dev);
}
void LIN_StopMotor(uint8_t * data_p){
	App_StopDev(Motor_dev);
}

void LIN_SetResistorTrim(uint8_t * data_p){
	uint16_t resValue = *data_p;
	NVM_WriteDataWord(nvm_ResistorTrim, 0, &resValue);
	sys_InitADC();
}

/* --------------------------------------------------------
 * 					ZONE Functions
 * ------------------------------------------------------- */

void LIN_ZONE_Start(uint8_t * data_p){
	App_StartDevice(Motor_dev);
}
void LIN_ZONE_Stop(uint8_t * data_p){
	App_StopDev(Motor_dev);

}

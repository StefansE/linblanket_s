/*
 * System.c
 *
 *  Created on: Dec 23, 2023
 *      Author: ewars
 */

#include "adc.h"
#include "NVM.h"
#include "NVM_DataId.h"
#include "AppStatus.h"
#include "System.h"

#define V_REFL_ADDR 	0x1FFFF7BA
#define V_REFH_ADDR 	0x1FFFF7BB

#define TS_CAL1_TEMP	30
#define TS_CAL2_TEMP	110

#define TS_CAL1_H		0x1FFFF7B8
#define TS_CAL1_L		0x1FFFF7B9
#define TS_CAL2_H		0x1FFFF7C2
#define TS_CAL2_L		0x1FFFF7C3

static uint16_t sys_ConvertMotorVoltage(uint16_t rawVal);
static uint16_t sys_ConvertSupplyVoltage(uint16_t rawVal);
static uint16_t sys_ConvertTemperature(uint16_t rawVal);

uint16_t ADC_Data[4] = {0};
uint16_t ADC_Volts[4] = {0};

float refFactor = 0;
float tempFactor = 0;

uint16_t vResistorValue_H = 62;
uint16_t reference = 0;
int32_t tsDiff = 0;
int32_t tsCal1 = 0;
int32_t tsCal2 = 0;

uint8_t sys_getTemperature(void){
	return (uint8_t) ADC_Volts[adc_Temperature];
}

void HAL_WWDG_EarlyWakeupCallback(WWDG_HandleTypeDef *hwwdg)
{
	//App_setDeviceStatus(Led2_dev, 100);
	HAL_Delay(100);
}

void sys_InitADC(void){

	uint8_t memRead_L = 0;
	uint8_t memRead_H = 0;

	// reading Mains divider trim value from NVM
	NVM_ReadEntryById(nvm_ResistorTrim, &vResistorValue_H);

	memRead_L = *((uint8_t*)V_REFH_ADDR);
	memRead_H = *((uint8_t*)V_REFL_ADDR);

	reference = (uint16_t)(memRead_L<<8) + (uint16_t)(memRead_H);

	//rawF = ((3.3 * reference * rawVal)/(ADC_Data[3] * 4095));
	refFactor = (3.3 * reference)/4095;

	memRead_L = *((uint8_t*)TS_CAL1_L);
	memRead_H = *((uint8_t*)TS_CAL1_H);

	tsCal1 = (int32_t)(memRead_L<<8) + (int32_t)(memRead_H);

	memRead_L = *((uint8_t*)TS_CAL2_L);
	memRead_H = *((uint8_t*)TS_CAL2_H);

	tsCal2 = (int32_t)(memRead_L<<8) + (int32_t)(memRead_H);

	tempFactor = (float)(TS_CAL2_TEMP - TS_CAL1_TEMP) / ((int32_t)tsCal2 - (int32_t)tsCal1);

}

void sys_ADC_handler(void){

	// Supply Voltage labeled "ADC_Input"
	ADC_Volts[adc_SupplyVoltage_33] = sys_ConvertSupplyVoltage(ADC_Data[adc_SupplyVoltage_33]);

	// Motor voltage labeled "ADC PWM Out"
	ADC_Volts[adc_MainsVoltage] = sys_ConvertMotorVoltage(ADC_Data[adc_MainsVoltage]);

	// Temperature conversion
	ADC_Volts[adc_Temperature] = sys_ConvertTemperature(ADC_Data[adc_Temperature]);

	HAL_ADC_Start_DMA(&hadc, (uint32_t*) &ADC_Data, 4);

}

uint16_t sys_ConvertTemperature(uint16_t rawVal){

	int32_t temperature = 0;

	//temperature = (tempFactor * (rawVal - tsCal1)) + TS_CAL1_TEMP;
	temperature = tempFactor * (rawVal - tsCal1);

	if(temperature >= 0){
		return (uint16_t) temperature;
	}
	else{
		return (uint16_t) -temperature;
	}
}


uint16_t sys_ConvertMotorVoltage(uint16_t rawVal){

	float rawF = 0;
	float voltage = 0;

	//conversion of ADC input
	// ADC_Data[3] is a measured internal reference voltage
	rawF = refFactor * rawVal/ADC_Data[adc_Reference];

	//conversion of voltage divider R = 62/ R1 = 10
	//U_we = (U_wy / R1)*(R + R1)
	voltage = ((rawF * (vResistorValue_H + 10))/10)*100;

	return (uint16_t) voltage;
}

uint16_t sys_ConvertSupplyVoltage(uint16_t rawVal){

	float rawF = 0;
	float voltage = 0;

	//conversion of ADC input
	// ADC_Data[3] is a measured internal reference voltage
	rawF = refFactor * rawVal/ADC_Data[adc_Reference];

	//conversion of voltage divider R = 62/ R1 = 10
	//U_we = (U_wy / R1)*(R + R1)
	voltage = (rawF * 2)*100;

	return (uint16_t) voltage;
}

void sys_ToggleLed(device_e device){

	if(APP_OK == App_getDeviceStatus(device)){
		App_setDeviceStatus(device, 0);

	}
	else{
		uint16_t duty = 0;
		NVM_ReadEntryById(NVM_GetDeviceEntryIndex((uint8_t) device), &duty);
		App_setDeviceStatus(device, duty);
	}
}

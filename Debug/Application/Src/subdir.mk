################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (11.3.rel1)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Application/Src/Application.c \
../Application/Src/LIN_Service.c \
../Application/Src/NVM.c \
../Application/Src/System.c 

OBJS += \
./Application/Src/Application.o \
./Application/Src/LIN_Service.o \
./Application/Src/NVM.o \
./Application/Src/System.o 

C_DEPS += \
./Application/Src/Application.d \
./Application/Src/LIN_Service.d \
./Application/Src/NVM.d \
./Application/Src/System.d 


# Each subdirectory must supply rules for building sources it contributes
Application/Src/%.o Application/Src/%.su Application/Src/%.cyclo: ../Application/Src/%.c Application/Src/subdir.mk
	arm-none-eabi-gcc -gdwarf-4 "$<" -mcpu=cortex-m0 -std=gnu11 -g3 -DDEBUG -DUSE_HAL_DRIVER -DSTM32F031x6 -c -I"C:/Users/ewars/OneDrive/Dokumenty/MyProjects/Lin_Slave/Application/Inc" -I../Drivers/STM32F0xx_HAL_Driver/Inc -I../Drivers/STM32F0xx_HAL_Driver/Inc/Legacy -I../Drivers/CMSIS/Device/ST/STM32F0xx/Include -I../Drivers/CMSIS/Include -I../Core/Inc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfloat-abi=soft -mthumb -o "$@"

clean: clean-Application-2f-Src

clean-Application-2f-Src:
	-$(RM) ./Application/Src/Application.cyclo ./Application/Src/Application.d ./Application/Src/Application.o ./Application/Src/Application.su ./Application/Src/LIN_Service.cyclo ./Application/Src/LIN_Service.d ./Application/Src/LIN_Service.o ./Application/Src/LIN_Service.su ./Application/Src/NVM.cyclo ./Application/Src/NVM.d ./Application/Src/NVM.o ./Application/Src/NVM.su ./Application/Src/System.cyclo ./Application/Src/System.d ./Application/Src/System.o ./Application/Src/System.su

.PHONY: clean-Application-2f-Src

